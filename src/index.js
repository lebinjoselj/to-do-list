import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const ToDoList = () =>{
  const [tasks,updateTask] = useState([
    {
      text:"learning react js is started",
      isCompleted:false
    },

    {
      text:"learning react js is in process",
      isCompleted:false
    },

    {
      text:"learning react js is completed",
      isCompleted:false
    }
  ]);

  return(
    <div className="tasklist">
      {tasks.map((task,index) =>(
        <div className="task-status">
          <span className="index">{index}</span>
          {task.text}
        </div>
      ))}
    </div>
  );

}

ReactDOM.render(<ToDoList />,document.getElementById('root'))
